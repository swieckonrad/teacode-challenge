import {
  AppBar,
  Checkbox,
  Container,
  Paper,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { useEffect, useState } from "react";
import "../App.css";

export const List = () => {
  const [members, setMembers] = useState([]);
  const [checked, setChecked] = useState(false);
  const [membersListById, setMembersListById] = useState([]);
  const [searchString, setSearchString] = useState("");

  useEffect(() => {
    fetch(
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    )
      .then((r) => r.json())
      .then((response) => setMembers(response));
  }, []);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  const sortedMembers = [...members].sort((a, b) => {
    if (a.last_name < b.last_name) {
      return -1;
    } else {
      return 1;
    }
  });

  const handleAddCheckedToList = (memberId) => {
    if (membersListById.includes(memberId)) {
      setMembersListById((previousState) =>
        previousState.filter((id) => id !== memberId)
      );
    } else {
      setMembersListById((previousState) => [...previousState, memberId]);
    }
  };

  console.log(membersListById);

  return (
    <div>
      <AppBar position="sticky">
        <Toolbar>
          <Typography variant="h5">Contacts</Typography>
        </Toolbar>
      </AppBar>
      <Container className="textfield">
        <TextField
          variant="outlined"
          label="Search user"
          onChange={(event) => setSearchString(event.target.value)}
        />
      </Container>
      {sortedMembers
        .filter((string) => {
          if (searchString === "") {
            return string;
          } else if (
            string.first_name
              .toLowerCase()
              .includes(searchString.toLowerCase()) ||
            string.last_name.toLowerCase().includes(searchString.toLowerCase())
          ) {
            return string;
          }
        })
        .map((member) => (
          <Paper className="paper" elevation={3}>
            <img className="img" src={member.avatar} alt="Avatar" />
            <Container className="container">
              {member.first_name} {member.last_name}
              <Checkbox
                value={checked}
                color="primary"
                onClick={(e) => {
                  handleChange(e);
                  handleAddCheckedToList(member.id);
                }}
              />
            </Container>
          </Paper>
        ))}
    </div>
  );
};
