import { List } from "./List/List";

const App = () => {
  return (
    <div className="app">
      <List />
    </div>
  );
};

export default App;
